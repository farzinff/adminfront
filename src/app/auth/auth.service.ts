import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {Subject} from 'rxjs/Subject';

const baseUrl = environment.apiUrl;

@Injectable()
export class AuthService {

  private loggedInSource = new Subject<string>();
  loggedIn$ = this.loggedInSource.asObservable();

  constructor(private http: HttpClient, private jwtHelperService: JwtHelperService) {
  }

  public getToken(): string {
    return this.jwtHelperService.tokenGetter();
  }

  public isAuthenticated(): boolean {
    const token: string = this.jwtHelperService.tokenGetter();

    if (!token) {
      return false;
    }

    const tokenExpired: boolean = this.jwtHelperService.isTokenExpired(token);

    return !tokenExpired;
  }

  public setCredentials(data) {
    localStorage.setItem('globals', JSON.stringify(data));

    const expiryTime = new Date().getTime() + 36000000;
    localStorage.setItem('expiryTime', expiryTime.toString());
    this.loggedInSource.next(data);
  }

  public clearCredentials() {
    localStorage.removeItem('globals');
    localStorage.removeItem('expiryTime');
    this.loggedInSource.next(null);
  }

  public login(params) {
    const body = new HttpParams()
      .set(`email`, params.username)
      .set(`password`, params.password);

    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})
    };

    return this.http.post(baseUrl + '/auth/login', body, httpOptions);
  }

  public forgetPassword(body) {
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    return this.http.get(baseUrl + '/admins/resetPasswordToken/'+body.email, httpOptions);
  }

  public resetPassword(body) {
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.put(baseUrl + '/admins/resetPassword', body, httpOptions);
  }

  public getUser() {
    return JSON.parse(localStorage.getItem('globals'));
  }
}

import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ApiRequestService} from "../../shared/services/api-request.service";
import {ToastrService} from 'ngx-toastr';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-admin',
  templateUrl: './add-admin.component.html',
  styleUrls: ['./add-admin.component.scss']
})
export class AddAdminComponent implements OnInit {

  user: any = {};
  busy: Subscription;

  constructor(public activeModal: NgbActiveModal, private toastrService: ToastrService, private apiService: ApiRequestService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.busy = this.apiService.create('/admins',this.user).subscribe(
      (doc:any) => {
        this.toastrService.success('Created successfully !! ');
        this.activeModal.close(doc.data || doc);
      }
    );
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {ApiRequestService} from '../../shared/services/api-request.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-edit-image',
  templateUrl: './edit-image.component.html',
  styleUrls: ['./edit-image.component.scss']
})
export class EditImageComponent implements OnInit {

  @Input() data;
  busy: Subscription;
  fileToUpload: File = null;
  previewUrl: string;

  constructor(public activeModal: NgbActiveModal, private toastrService: ToastrService, private apiService: ApiRequestService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.busy = this.apiService.putFile(this.data.path, this.fileToUpload).subscribe(
      (doc: any) => {
        this.toastrService.success('Updated successfully !! ');
        this.activeModal.close(doc.data || doc);
      }
    );
  }

  handleFileInput(event) {
    if (event.target.files && event.target.files.item(0)) {
      this.fileToUpload = event.target.files.item(0);

      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.previewUrl = event.target.result;
      };
      reader.readAsDataURL(this.fileToUpload);
    }
  }

}

import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {ApiRequestService} from '../shared/services/api-request.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any;
  busy: boolean;
  busySecurity: boolean;
  isError: boolean;
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;


  constructor(public auth: AuthService, public apiService: ApiRequestService,
              private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.user = this.auth.getUser() || {};
  }

  onSubmit() {
    this.busy = true;
    this.apiService.update('/admins/' + this.user._id, {email: this.user.email, name: this.user.name}).subscribe(data => {
      this.toastrService.success('Updated successfully !!');
      this.busy = false;
    }, err => {
      this.busy = false;
    });
  }

  onSubmitSecurityForm(form) {
    this.isError = false;

    if (this.newPassword !== this.confirmPassword) {
      this.isError = true;
      return false;
    }

    this.busySecurity = true;
    this.apiService.update(`/admins/changePassword`, {
      password: this.oldPassword,
      newPassword: this.newPassword
    }).subscribe(data => {
      this.toastrService.success('Updated successfully !!');
      form.reset();
      this.busySecurity = false;
    }, err => {
      this.busySecurity = false;
    });
  }
}

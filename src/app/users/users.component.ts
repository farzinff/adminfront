import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ApiRequestService} from '../shared/services/api-request.service';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ConfirmDialogComponent} from "../shared/components/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  data: any = {
    pagination: {
      page: 1,
      pageSize: 25
    },
    list: [],
    busy: Subscription
  };

  constructor(private cdRef: ChangeDetectorRef, private apiService: ApiRequestService, private modalService: NgbModal) {
    this.data.search = function (params) {
      return apiService.get('/admins', params);
    }
  }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  openDeleteDialog(id, index) {
    const data = Object.assign({}, {_id: id, path: '/admins'});
    const modalRef = this.modalService.open(ConfirmDialogComponent, {windowClass: 'mt-10'});
    modalRef.componentInstance.data = data;

    modalRef.result.then((result) => {
      if (result) {
        this.data.list.splice(index, 1);
        this.data.pagination.total -= 1;
      }
    });

  }

}
